#!/usr/bin/env node

var fs = require("fs");
const yargs = require('yargs');
var yaml = require('js-yaml');
var raml = require("raml-1-parser");

const { exec } = require('child_process');

const argv = yargs
  .usage('Usage: raml2postman -i [input_file]')
  .version('1.0.5')

  .alias('v', 'version')

  .string('i')
  .alias('i','input')
  .describe('i','Input raml file to use')

  .string('o')
  .alias('o','output')
  .describe('o','Name of output file - can use relative pathing')

  .help('h')
  .alias('h', 'help')
  .argv;


let input = argv.input
let output = argv.output;

let currDir = process.cwd();


if(!output){
  output = currDir + "/postman_collection.json";
}
else{
  output = currDir + "/" + output;
}

var tmpFile = __dirname + "/.tmp-out.json";

if(!input){
  if(argv._.length !==1){
    console.error('Error: You need to specify the RAML input file\n');
    yargs.showHelp();
    process.exit(1);
  }
}
else{

  var api = raml.loadSync(input);

  fs.writeFile(tmpFile, JSON.stringify(api, null, 2) , (err) => {
    if (err) console.log(err);
    console.log("Successfully written tempfile");
  });


//  console.log("currDir:" + currDir);
  console.log("output file:" + output);

  console.log("Executing cmd-dw. Output: " +output);
  exec("cd " + __dirname +"/cmd-dw && mvn exec:java -Dexec.mainClass=\"com.hughesportal.cmd_dw.Main\" -Dexec.args=\"-d ../dataweave.dwl -p " + tmpFile +  " -o " + output +" \"",
            (err, stdout, stderr) =>{
    if(err){
      console.log("Error running cmd2dw:\n" + err);
      return;
    }
    console.log("Successfully written to " + output);
  });

//   fs.unlinkSync(tmpFile);

}

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}
