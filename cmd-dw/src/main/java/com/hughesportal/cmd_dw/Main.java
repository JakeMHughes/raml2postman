package com.hughesportal.cmd_dw;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Hello world!
 *
 */
public class Main 
{
	static DataWeave dw;
    public static void main( String[] args )
    {
    	String dwScript = null;
    	String payload = null;
    	String out = null;
        for(int i =0; i<args.length; i++) {
        	
        	if(args[i].equals("-d")) {
        		i++;
        		dwScript = args[i];
        	}
        	else if(args[i].equals("-p")) {
        		i++;
        		payload = args[i];
        	}
        	else if(args[i].equals("-o")) {
        		i++;
        		out = args[i];
        	}
        	else{
        		printHelp();
        		System.exit(0);
        	}
        	
        }
        
        if(dwScript == null || payload == null) {
        	System.out.println("Error: You must provide a payload and dataweave script!\n\n");
        	printHelp();
        	System.exit(0);
        }
        //end arguments
        
        System.out.println("BEGIN:\n");
        dw = new DataWeave(readFile(dwScript), readFile(payload));
        String transform = dw.getDataWeave();
        System.out.println("END");
        
        if(out == null) {
        	System.out.println(transform);
        }
        else {
        	try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    	new FileOutputStream(out), "utf-8"))) {
		         writer.write(transform);
		    } catch (IOException e) {
		    	System.out.println("Failed writing to file: "+ out+"\n\nError: "+e.getMessage()+ "\n\nExiting...");
		    	System.exit(1);
			}
        }
    }
    
    private static String readFile(String file) {
    	String val= "";
		try {
			 BufferedReader reader = new BufferedReader(new FileReader(file));
			 String line;
			 while((line = reader.readLine()) != null) {
				 val += line+"\n";
			 }
		} catch (IOException e) {
			System.out.println("Failed reading file: "+ file+"\n\nError: " + e.getMessage() + "\n\nExiting...");		
			System.exit(1);
		} 
    	
    	return val;
    }
    
    private static void printHelp() {
		System.out.println(
				"-d,\t\tSets the dataweave file.\n"+
				"-p,\t\tSets the payload file.\n"+
				"-o,\t\t(Optional) Sets the output file. Prints by default\n"+
				"-h,\t\tPrints the help menu."
		);
    }
}
