package com.hughesportal.cmd_dw;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;

import org.mule.runtime.api.el.BindingContext;
import org.mule.runtime.api.metadata.DataType;
import org.mule.runtime.api.metadata.MediaType;
import org.mule.runtime.api.metadata.TypedValue;
import org.mule.runtime.core.api.util.IOUtils;
import org.mule.runtime.core.internal.el.DefaultBindingContextBuilder;
import org.mule.weave.v2.el.WeaveExpressionLanguage;

public class DataWeave {

	private String dw;
	private String payload;
	private String mimeType;
    private WeaveExpressionLanguage weaveEngine;
    public static final String PAYLOAD = "payload";
	
	public DataWeave(String dw, String payload) {
        weaveEngine = new WeaveExpressionLanguage();
        
		this.dw = dw;
		this.payload = payload;
		
		Reader inputString = new StringReader(dw);
		try {
			BufferedReader reader = new BufferedReader(inputString);
			
			String line;
			while((line = reader.readLine()) != null) {
				if(line.contains("output")) {
					mimeType = line.replace("output ", "");
					System.out.println("This script is outputting: "+ mimeType);
				}
			}
			
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
    private DataType getInputDataType(String mimeType) {
        return DataType.builder()
                .type(String.class)
                .mediaType(MediaType.parse(mimeType))
                .build();
    }
    
    private String getTypedValueStringValue(TypedValue<?> evaluate) {
        Object value = evaluate.getValue();

        String textToShow;
        if(value instanceof InputStream){
            textToShow = IOUtils.toString((InputStream) value);
        } else {
            textToShow = value.toString();
        }
        return textToShow;
    }
    
    
    public String getDataWeave() {
        TypedValue inputTypedValue = new TypedValue<>(payload, getInputDataType(mimeType));
        
        BindingContext bindingContext = new DefaultBindingContextBuilder()
                .addBinding(PAYLOAD, inputTypedValue)
                .build();
        
        try {
            TypedValue<?> evaluate = weaveEngine.evaluate(dw, bindingContext);
            return (getTypedValueStringValue(evaluate));
        } catch (Exception e) {
            return e.getMessage();
        }
        //return "empty";
    }
}
