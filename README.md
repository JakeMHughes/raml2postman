# raml2postman
## Summary
Raml to Postman is a small project that takes in a RAML file and converts it to a postman collection that can be imported.  
It works with the use of a few libraries. The NodeJS script takes in a RAML file and using the raml-1-parser library it  
converts it to a json object. The script then executes the Java cmd-dw program which takes in a payload (RAML json) and a  
dataweave script. Then, using the two files it executes the script in the dataweave file against the payload file.  
The script provided is made special for a RAML JSON payload to be converted to a postman collection.
### Output
* Every endpoint has a folder with its methods and any sub-endpoint folders
* Bodies use the listed example under body tag fisrt if it exists
	* If not, it uses the defined type
		* If not it is empty
* Query parameters, Uri parameters, and headers all use variables named after the RAML name
	* i.e. `headers: test: ...` creates a variable `{{test}}`
	* Note: The variables are not defined by default
## Usage
### Requires
* fs
* yargs
* js-yaml
* raml-1-parser
* Java 8

### Install
#### Manual
* Clone the project where ever you would like  
* Install the package with `npm install -g /path/to/download/`  

#### NPM
* Install the project with `npm install -g raml2postman`

### Commands

* Usage: `raml2postman [options]`
* Usage: `raml2postman -i [input file] -o [output file]`
* Show help menu: `-h --help`
* Show version number: `-v --version`
* Set input file: `-i --input`
* Set output file: `-o --output`
	* Bear in mind: Output tag can only be used with relative pathing currently
